# Delightful Sustainable VPS
[![delightful](https://codeberg.org/teaserbot-labs/delightful/media/branch/main/assets/delightful-badge.png)](https://codeberg.org/teaserbot-labs/delightful)

A list of sustainable VPS (Virtual Private Servers) providers.

## What classify as sustainable?
Paraphrasing [Wikipedia](https://en.wikipedia.org/wiki/Sustainability), sustainability is the capacity of Earth's biosphere and human civilization to co-exist, through the domains of environment, economy and society.

Here, we are classifying sustainable VPS providers if the provider meets some of these principles: 
- Usage of renewable energy
- Worker owned business, non-profit
- Focus on open source technologies
- Slow, small, organic and conscious growth
- Care for user privacy

It should not fall into:
- Hyperscalers, such as AWS, GCP, Azure, Digital Ocean etc
- Subsidiary of big corporations
- Inorganic growth
- Privacy invasive

## The list
Describing VPS providers is tricky. There are too many things to factor in, like the servers location, pricing, plans, operational system, energy sources, business model, support etc. Not even talking about technical features, like backups, access interface, IPv4/IPv6, GPU etc etc etc.

Trying to list all the information available (or unavailable) would create an unfair list.

With that in mind, I decided to include only the name and location (preferably city and country). Feel free to explore on your own.

| Name | Location | 
| ---  | -------- | 
| [1984](https://1984.hosting/) | Iceland |
| [1fire](https://1fire.de/) | Germany |
| [Alwyzon](https://alwyzon.com) | Vienna, Austria |
| [Capsul](https://capsul.org/) | Atlanta, US |
| [Dogado](https://www.dogado.de) | Düsseldorf, Germany |
| [eclip.is](https://eclips.is) | Netherlands |
| [FlokiNET](https://flokinet.is) | Romania, Iceland, Finland |
| [Greenhost](https://greenhost.net/) | Netherlands |
| [GreenWebspace](https://greenwebspace.com) | Vienna, Austria |
| [Hostsharing eG](https://www.hostsharing.net/) | Germany |
| [IO Cooperative](https://iocoop.org/services/virtual-private-servers/) | Fremont, US |
| [leafcloud](https://leaf.cloud) | Netherlands |
| [May First Movement Technology](https://mayfirst.coop/en/member-benefits/) | CA, US |
| [MediaBlaze](https://mediablazehosts.coop) | UK |
| [Njalla](https://njal.la) | Sweden |
| [Nubla.fr](https://nubla.fr) | South West France |
| [Quality Localtion](https://www.qloc.de) | Germany |
| [Seeweb](https://www.seeweb.it) | Italy, Switzerland |
| [servers.coop](https://servers.coop/) | ? |
| [Stuxhost](https://stuxhost.com/store/vps-hosting) | Netherlands |
| [Tilaa](https://tilaa.com) | Netherlands |
| [Tranquillity](https://tranquillity.se/) | Sweden |
| [Webarchitects Co-operative](https://www.webarchitects.coop/virtual-servers) | Sheffield, UK | 
| [Windcloud](https://www.windcloud.de/produkte/vps) | Germany | 

## Maintainers
If you have questions or feedback regarding this list, then please create an [Issue in our tracker](https://codeberg.org/jonatasbaldin/delightful-sustainable-vps/issues), and optionally @mention one or more of our maintainers:
- [@jonatasbaldin](https://codeberg.org/jonatasbaldin)

## Contributors
With delight we present you some of our [delightful contributors](delightful-contributors.md) (please add yourself if you are missing).

## License
[CC 4.0 International](LICENSE).

## Acknowledgements
Thanks [Fediverse](https://indieweb.social/@jonatasbaldin/106862654617983230) for the initial contributions ❤️